variable "project" {
  type    = string
  default = "my-project"
}
variable "gke_master_ipv4_cidr_block" {
  type    = string
  default = "172.23.0.0/28"
}

variable "region" {
  type = string
  default = "us-east1"
}

variable "zone" {
  description = "ID of GCP Zone used by each node."
  type        = list(string)
}

variable "authorized_source_ranges" {
  type        = list(string)
  description = "Addresses or CIDR blocks which are allowed to connect to GKE API Server."
}

variable "mysql_location_preference" {
  type = string
  default = "us-east1"
}

variable "mysql_machine_type" {
  type = string
  default = "db-n1-standard-2"
}

variable "mysql_database_version" {
  type = string
  default = "MYSQL_8_0"
}

variable "mysql_default_disk_size" {
  type = string
  default = "100"
}

variable "mysql_availability_type" {
  type = string
  default = "REGIONAL"
}

variable "db_username" {
  type = string
}

variable "db_password" {
  type = string
}

variable "port" {
  type = string
}

variable "cluster_name" {
  type = string
  default = "my-cluster"
}

variable "service_account" {
  description = "Name of service account for redis VM instances being created"
  type        = string
}
variable "bucket_name" {
  description = "Name of GCS bucket containing redis configuration files"
  type        = string
}
variable "health_check_uri" {
  description = "URI of health check for managed instance group being created (format=projects/*/global/healthChecks/*)"
  type        = string
}
variable "instance_type" {
  description = "Name of instance type for redis VM instances being created"
  type        = string
}
variable "disk_size_gb" {
  description = "Size of persistent disk attached to redis VM instances being created"
  type        = number
  default     = 1
}
variable "pass" {
  description = "Redis password."
  type        = string
}